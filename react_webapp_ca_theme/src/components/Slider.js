import React, {Component} from 'react'

//background color changes in the interval of 3 secs when the player button is ON.
class Slider extends Component{
    constructor(){
        super()
        this.state = {
            color:'black',
            stop:false,
            count:0,
            playerOn : true,
            btnborder: [null, null, null], //initial setting of border style of all buttons to null.
            title:'',
            titleColor:'',
        }
        this.onClickSlider = this.onClickSlider.bind(this)
        this.changeBackGround = this.changeBackGround.bind(this)
        this.colorButtons = this.colorButtons.bind(this)
        this.resetColor = this.resetColor.bind(this)
        let timeOutFn
    }
    componentDidMount(){
        this.onClickSlider()
    }
    componentWillUnmount(){
        this.stopSlider()  //to remove the timeout functions.
    }

    colorArray = ['#000', ' #335075', '#ad3e3e']
    titles = ['Banff National Park', 'Algonquin Provincial Park', 'Rideau Canal']
    titleColors=['#364356','#2f4059','#2a3c56']


    changeBackGround(){
        console.log('main', this.state.stop)
        if(!this.state.stop){
            this.setState({
                color:this.state.color,
                stop:!this.state.stop},()=>console.log(this.state))

            let current = this.timeOutFn
            console.log('clear timeout', current, this.state.color)
            this.stopSlider()
        }else{
            this.setState({stop:!this.state.stop})
            this.onClickSlider()
        }
    }


    onClickSlider(){
        let timer = 1000

        for(let i = this.state.count; i <= this.colorArray.length && timer < 30000; i++, timer += 3000){
            if(i===this.colorArray.length){
                console.log('i is ',i)
                i=0
            }
            let arr = [null,null,null].map((ele, index)=>{
                if(index===i){
                    return '0.5em solid #75787c'
                }
                return ele
            })

            this.timeOutFn = window.setTimeout(function(){
                this.setState({
                    color:this.colorArray[i],
                    count:i,
                    btnborder:arr,
                    title: this.titles[i],
                    titleColor: this.titleColors[i]

                })

            }.bind(this), timer)
            console.log('loop')
            console.log('color test', this.colorArray[i], i, this.state)

        }
        console.log('end')
    }
    resetColor(i){
        console.log('the i value on clickk is',i)
        let arr = [null,null,null].map((ele, index)=>{
            if(index===i){
                return '0.5em solid #75787c'
            }
            return ele
        })

        this.setState({
            btnborder:arr,
            title:this.titles[i],
            titleColor:this.titleColors[i]
        })

    }

    colorButtons(){
       var btnGroup = []
       for(let i=0; i< 3; i++){
            btnGroup.push(
                <button onClick={()=> {
                    this.setState({
                        color:this.colorArray[i],
                        stop:true
                    })
                    this.stopSlider()
                    this.resetColor(i)
                }}
                style={{background:this.colorArray[i],
                    border:this.state.btnborder[i]
                }}></button>
            )
       }
       return btnGroup
    }

    stopSlider=()=> {
        if(this.timeOutFn > 0){
            while (this.timeOutFn--){
                clearTimeout(this.timeOutFn)
            }
        }
        return null

    }

    render(){
        return(
            <div className="slider-container">
                <div className= "slider-image" style={{background: this.state.color}}>
                    1170 x 347
                </div>
                <div className="slider-image-title"
                     style={{background:this.state.titleColor}}><a hr>{this.state.title}</a></div>
                <div className="slider-buttons">
                    <div className="slider-buttons-image">
                        {this.colorButtons()}
                    </div>
                    <div className="slider-buttons-player">
                        <button onClick={()=> {
                            this.changeBackGround()
                            this.setState({playerOn:!this.state.playerOn})
                        }}>
                            {this.state.playerOn ? <i class="fas fa-pause"> Pause</i> : <i class="fas fa-play"> Play</i>}
                        </button>
                    </div>
                </div>

            </div>
        )
    }
}

export default Slider