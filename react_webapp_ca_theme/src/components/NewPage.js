import React, {Component} from 'react'
import {Link} from 'react-router-dom'

//currently: dummy component to display static content

class NewPage extends Component{

    trim(){
        let url = window.location.href.split('/')[window.location.href.split('/').length - 1]
        return url.split('%20').join(' ')
    }
    render(){
        return(
            <div>
                <div className="home-link"><Link to="/">Home</Link></div>
                <div className="welcome-content">Welcome to <strong><em>{this.trim()}</em> </strong> Page!</div>
            </div>
        )
    }
}

export default NewPage