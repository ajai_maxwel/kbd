import React, { Component } from 'react';
import {BrowserRouter, Route } from 'react-router-dom'
import {Link} from 'react-router'
import Header from './Header'
import Banner from './Banner'
import Slider from './Slider'
import Content from './Content'
import Footer from './Footer'
import NewPage from './NewPage'

class App extends Component {
    render() {
        return (
            <BrowserRouter>
                <div className="app-main">
                    <div className="app-lang"></div>
                    <Banner />
                    <Header />
                    <Route exact path = "/" component={Slider} />
                    <Route exact path = "/" component={Content} />
                    <Route exact path = "/" component={Footer} />
                    <Route exact path = "/:id" component={NewPage} />
                </div>

            </BrowserRouter>

        );
    }
}

export default App;