import React, {Component} from 'react'
import {Link} from 'react-router-dom'
class Banner extends Component{

    imageUrl = "https://www.swc-cfc.gc.ca/images/goc-gdc-sig.jpg"

    render(){
        return(
            <div className="banner-container">
                <div className="banner-image">
                    <Link to="/" style={{cursor:'pointer'}}>
                        <img src={this.imageUrl} alt="Government of Canada Logo" onClick={()=> null} />
                    </Link>
                </div>
                <div className="banner-searchbar" >
                    <input className="banner-searchbar" placeholder="Search Canada.ca"></input>
                    <i className="fas fa-search banner-searchicon" style={{cursor:'pointer'}}></i>
                </div>
            </div>
        )
    }
}

export default Banner