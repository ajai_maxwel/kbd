import React, {Component} from 'react'

class Content extends Component{
    constructor(){
        super()
        this.listContents = this.listContents.bind(this)
    }
    // to be replaced with query results from DB.
    contents = [{
        title: "Find a Job",
        content: "Find public and private sector job opportunities and hiring programs, apply or extend a work permit, get a Social Insurance Number, a criminal record check or a security clearance."
        },
        {
            title:'Canada Pension Plan retirement pension',
            content:'Information on eligibility criteria, deciding when to take your pension, how to apply online and amounts.'

        },
        {
            title:"Weather",
            content:"Information on current conditions, short and long-term forecasts and public weather alerts, access marine forecasts, the Air Quality Health Index and historical climate data."

        },
        {
         title: "Employment Insurance",
         content:"Information about Employment Insurance (EI) temporary benefits for workers, sickness, fishing and family-related benefits as well as how to apply online and submit a report."
        },
        {
            title:"Old Age Security pension",
            content:"Information on a pension you can receive if you are 65 years of age or older and have lived in Canada for at least 10 years - even if you have never worked."
        },
        {
            title: "Join the Armed Forces",
            content: "Information about jobs with the Canadian Armed Forces, wages, benefits and recruitment centre locations."
        },
        {
            title:"My Account for individuals – Canada Revenue Agency",
            content: "Information on how the service works, how to register for it, and what you can do online."
        },
        {
            title: "Get a passport",
            content: "Includes passport applications, passport offices and processing times for renewals and new applications."
        },
        {
            title:"Business Planning",
            content:"Includes checklists and guides for starting a business, business planning tips, and templates and sample business plans."
        }
    ]

    focus = [{
        title: 'Audience',
        content: "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eaque praesentium, nulla ab nobis architecto repellendus hic iure laboriosam."
        },
        {
            title: 'Audience',
            content: "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eaque praesentium, nulla ab nobis architecto repellendus hic iure laboriosam."
        },
        {
            title: 'Audience',
            content: "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eaque praesentium, nulla ab nobis architecto repellendus hic iure laboriosam."
        }
    ]

    listContents(datas){
        return datas.map(data=>{
            return <div className="content-subitems">
                    <a href="#"><h4>{data.title}</h4></a>
                    <p>{data.content}</p>
            </div>
        })
    }
    render(){
        return(
            <div className="content-main">
                <div className="content-main-heading">
                    <h2 className="content-main-heading-title">Most requested services and information</h2>
                    <p >Get quick, easy access to the Government of Canada's most requested services and information.</p>

                </div>
                <div className="content-container">
                    {this.listContents(this.contents)}
                </div>
                <div className="content-more"><a href="#"><strong>All services </strong></a> </div>
                <div className="content-focus">
                    <h2 className="content-focus-title">Focus on</h2>
                    <div className="content-focus-article">
                        {this.listContents(this.focus)}
                    </div>
                    <div className="content-date">
                        Date modified:
                        2017-12-05
                    </div>
                </div>


            </div>

        )
    }
}

export default Content