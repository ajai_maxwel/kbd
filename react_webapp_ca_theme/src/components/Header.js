import React, {Component} from 'react'
import DropDownMenu from "./DropDownMenu";

class Header extends Component{

    renderContent() {
        let menu = ['Jobs', 'Immigration', 'Travel', 'Business', 'Benefits', 'Health', 'Taxes', 'More Services']
        let submenu = [
            ['Job Bank', 'Find job','Training', 'Starting a business', 'Workplace Standards','Pension and retirements','Employment insurance', 'Hire and manage employees', 'Jobs more'],
            ['My Applicaion', 'Immigration','Visit','Work','Study','Enforcement and violation', 'Immigration more'],
            ['Travel Advise and Advisories','Air Travel', 'Travel more']
        ]

        let menuBar = []
        for(let i = 0; i < menu.length; i++){
            menuBar.push(<DropDownMenu menu = {menu[i]} submenu = {submenu[i]}/>)
        }
        return menuBar
    }

    render(){
        return(
            <header>
                <nav className="app-header-nav">
                    <div className="app-dummy-decorator"></div>
                    {this.renderContent()}
                    <div className="app-dummy-decorator"></div>
                </nav>
            </header>
        )
    }
}

export default Header;