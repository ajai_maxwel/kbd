import React, {Component} from 'react'
import {Link} from 'react-router-dom'

class DropDownMenu extends Component{
    constructor(props){
        super(props)
        this.state = {
            menu: this.props.menu,
            submenu : this.props.submenu,
            flag: false
        }

    }

    render(){
        return(
            <div className="drop-container"
                 onMouseEnter={()=>this.setState({flag:true})}
                 onMouseLeave={()=>this.setState({flag:false})} >

                <div className="drop-header" style={{background: this.state.flag? '#273156': null , cursor:'pointer'}}>
                    <div className="drop-menu"
                         onClick={()=>this.setState({
                        flag:!this.state.flag })} >
                        { this.state.menu}
                        {this.state.flag || this.state.submenu === undefined
                            ? <Link to="/" style={{cursor:'pointer'}} />
                            : <i className="fas fa-angle-down drop-icon"></i>}
                    </div>

                </div>

                <div className="drop-list"  >
                        {this.state.flag && this.state.submenu ? this.state.submenu.map( item => {
                            return <div className="drop-list-submenu" key={item.toString()}><Link to={"/"+item} style={{cursor:'pointer'}}>{item}</Link></div>
                        }) : null}
                </div>
            </div>
        )
    }
}

export default DropDownMenu;