import React, {Component} from 'react'

class Footer extends Component{
    moreinfo = ['Contact us', 'Departments and agencies', 'Public service and military','News', 'Treaties, laws and regulations', 'Government-wide reporting', 'Prime Minister', 'How government works', 'Open government']

    footerLinks = ['Social Media', 'Mobile Applications', 'About Canada.ca', 'Terms and Conditions', 'Privacy']
    listit = () => {
        return this.moreinfo.map(data =>{
            return <div className="footer-container-content-links"><a href="#">{data}</a></div>
        })
    }

    listOut(){
        let len = (this.footerLinks.length * 2 )-1
        //len takes in both circle(decoration) and link-words
        let footerContent = []
        let j=0
        for(let i = 0; i<len; i++){

            if(i%2==0){
                footerContent.push(<div style={{ cursor: 'pointer'}}>{this.footerLinks[j]}</div>)
                j++
            }else{
                footerContent.push(<i className="fas fa-circle"></i>)
            }
        }
        return footerContent
    }
    render(){
        return(
            <div className="footer-main">
                <div className="footer-btn">
                    <button className="footer-btn-report"><i className="fas fa-play"></i> Report a problem on the page</button>
                    <button className="footer-btn-share"><i className="fas fa-share-square"></i>Share this page</button>
                </div>
                <div className="footer-main-content">
                    {this.listit()}
                </div>
                <footer className="footer-container">
                    <div className="footer-container-content" >{this.listOut()}</div>
                    <img className="footer-container-logo" src="http://www.international.gc.ca/world-monde/assets/images/funding-financement/Canwordmark_colour.png" alt="Canada logo" />
                </footer>


            </div>
        )
    }
}

export default Footer
